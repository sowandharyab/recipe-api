%dw 1.0
%output application/json
---
{
	recipes: payload.recipes map {
		publisher: $.publisher,
		f2f_url: $.f2f_url,
		title: $.title,
		source_url: $.source_url,
		recipe_id: $.recipe_id,
		image_url: $.image_url,
		social_rank: $.social_rank,
		publisher_url: $.publisher_url
	} filter (lower $.title contains "egg") == false
}