﻿Recipe Api

This API is used to search for recipe's relating to a query and get the recipe details

Recipe Search:

Endpoint : https://recipeapi.us-e2.cloudhub.io/recipe/search?client_id=a3a069730fe74921896c4079a0af7eba&client_secret=32942B02025940019b0E389d77cc1ce0&ingredients=egg&pageSize=1

Notes:

1) This api is enforced with client id and secret and passing client id and secret values are important.
2) If recipes related to multiple items has to searched then it can be given as comma seperated values in ingredients. 
Example - chicken,egg
3) This api does not support recipe search only for egg.
4) If in case multiple items are provided as search string then the recipes containing egg will not be displayed in the response.
5) There are limitation for the backend api. As i have used free trail only 50 capi calls per day is allowed.
6) I have filtered egg recipes based on title.

Get Recipe:

Endpoint - https://recipeapi.us-e2.cloudhub.io/recipe/{recipeId}?client_id=a3a069730fe74921896c4079a0af7eba&client_secret=32942B02025940019b0E389d77cc1ce0

Notes:

1) This api is enforced with client id and secret and passing client id and secret values are important.
2) If a recipe contains egg as an ingredient then error is reported stating "Recipe contains egg" else result is displayed. 
3) There are limitation for the backend api. As i have used free trail only 50 capi calls per day is allowed.


Folder details:

recipe-api - contains code for implementation
recipe-api-proxy - contains code for proxy implementation
